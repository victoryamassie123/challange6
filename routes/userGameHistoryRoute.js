const express = require('express');
const userGameHistoryController = require('../controller/userGameHistoryController');

const router = express.Router()

router.get('/', userGameHistoryController.getHistory);
router.post('/', userGameHistoryController.createHistory);
router.patch('/updateHistory/:id', userGameHistoryController.updateHistory);
router.delete('/deleteHistory/:id', userGameHistoryController.deleteHistory);

module.exports = router;