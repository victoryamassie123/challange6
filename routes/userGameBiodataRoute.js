const express = require('express');
const userGameBiodataController = require('../controller/userGameBiodataController');

const router = express.Router();

router.get('/', userGameBiodataController.getBiodata);
router.post('/', userGameBiodataController.createBiodata);
router.patch('/updateBiodata/:id', userGameBiodataController.updateBiodata);
router.delete('/deleteBiodata/:id', userGameBiodataController.deleteBiodata);

module.exports = router;