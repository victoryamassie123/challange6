const express = require('express');
const userGameController = require('../controller/userGameController');

const router = express.Router();

router.get('/', userGameController.getUser);
router.post('/', userGameController.createUser);
router.patch('/updateUser/:id', userGameController.updateUser);
router.delete('/deleteUser/:id', userGameController.deleteUser);


module.exports = router;