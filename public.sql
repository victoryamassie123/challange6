/*
 Navicat Premium Data Transfer

 Source Server         : Postgres_Local
 Source Server Type    : PostgreSQL
 Source Server Version : 140003
 Source Host           : localhost:5432
 Source Catalog        : user_game
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140003
 File Encoding         : 65001

 Date: 09/11/2022 19:40:14
*/


-- ----------------------------
-- Sequence structure for auto_increment
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auto_increment";
CREATE SEQUENCE "public"."auto_increment" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auto_increment_biodata
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auto_increment_biodata";
CREATE SEQUENCE "public"."auto_increment_biodata" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auto_increment_history
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auto_increment_history";
CREATE SEQUENCE "public"."auto_increment_history" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for user_game
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_game";
CREATE TABLE "public"."user_game" (
  "id" int4 NOT NULL DEFAULT nextval('auto_increment'::regclass),
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "password" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of user_game
-- ----------------------------
INSERT INTO "public"."user_game" VALUES (4, 'test', 'password');
INSERT INTO "public"."user_game" VALUES (5, 'test 2', 'password');
INSERT INTO "public"."user_game" VALUES (6, 'test 3', 'password');
INSERT INTO "public"."user_game" VALUES (7, 'test 4', 'password');
INSERT INTO "public"."user_game" VALUES (8, 'Bambang', 'bambang');

-- ----------------------------
-- Table structure for user_game_biodata
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_game_biodata";
CREATE TABLE "public"."user_game_biodata" (
  "id" int4 NOT NULL DEFAULT nextval('auto_increment_biodata'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "phone" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "id_user" int4
)
;

-- ----------------------------
-- Records of user_game_biodata
-- ----------------------------
INSERT INTO "public"."user_game_biodata" VALUES (1, 'Bambang Pamungkas', '087655463743', 'bandung', 4);

-- ----------------------------
-- Table structure for user_game_history
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_game_history";
CREATE TABLE "public"."user_game_history" (
  "id" int4 NOT NULL DEFAULT nextval('auto_increment_history'::regclass),
  "waktu" date,
  "score" varchar(255) COLLATE "pg_catalog"."default",
  "id_user" int4
)
;

-- ----------------------------
-- Records of user_game_history
-- ----------------------------
INSERT INTO "public"."user_game_history" VALUES (2, '2022-11-09', '80', 4);
INSERT INTO "public"."user_game_history" VALUES (3, '2022-11-09', '100', 4);
INSERT INTO "public"."user_game_history" VALUES (4, '2022-11-09', '80', NULL);
INSERT INTO "public"."user_game_history" VALUES (5, '2022-11-09', '80', 4);
INSERT INTO "public"."user_game_history" VALUES (1, '2022-11-09', '60', 4);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."auto_increment"
OWNED BY "public"."user_game"."id";
SELECT setval('"public"."auto_increment"', 8, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."auto_increment_biodata"
OWNED BY "public"."user_game_biodata"."id";
SELECT setval('"public"."auto_increment_biodata"', 1, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."auto_increment_history"
OWNED BY "public"."user_game_history"."id";
SELECT setval('"public"."auto_increment_history"', 5, true);

-- ----------------------------
-- Primary Key structure for table user_game
-- ----------------------------
ALTER TABLE "public"."user_game" ADD CONSTRAINT "user_game_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_game_biodata
-- ----------------------------
ALTER TABLE "public"."user_game_biodata" ADD CONSTRAINT "user_game_biodata_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_game_history
-- ----------------------------
ALTER TABLE "public"."user_game_history" ADD CONSTRAINT "user_game_history_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table user_game_biodata
-- ----------------------------
ALTER TABLE "public"."user_game_biodata" ADD CONSTRAINT "fk_id_user" FOREIGN KEY ("id_user") REFERENCES "public"."user_game" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_game_history
-- ----------------------------
ALTER TABLE "public"."user_game_history" ADD CONSTRAINT "fk_id_user" FOREIGN KEY ("id_user") REFERENCES "public"."user_game" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
