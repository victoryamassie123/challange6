const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const bodyParser = require('body-parser');
const userRouter = require('./routes/userGameRoute');
const historyRouter = require('./routes/userGameHistoryRoute');
const biodataRouter = require('./routes/userGameBiodataRoute');

const app = express();
const port = 3000;

//menggunakan body-parser
app.use(bodyParser.json());

// menggunakan ejs
app.set('view engine', 'ejs');
app.use(expressLayouts);


app.use('/api/v1/users', userRouter);
app.use('/api/v1/histories', historyRouter);
app.use('/api/v1/biodata', biodataRouter);


module.exports = app;