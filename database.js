const Pool = require('pg').Pool;

const pool = new Pool({
    user: "postgres",
    password: 'superadmin',
    database: 'user_game',
    host: 'localhost',
    port: 5432,
});

module.exports = pool;