const pool = require('../database');

exports.getHistory = async (req, res) => {
    try {
        const AllHistory = await pool.query("Select * from user_game_history");
        console.log(AllHistory.rows);
        res.status(200).json({
            status: 'Success',
            data: AllHistory.rows
        });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        })
    }
}
exports.createHistory = async (req, res) => {
    try{
        const {waktu, score, id_user} = req.body;
        const NewHistory = await pool.query("Insert into user_game_history (waktu, score, id_user) Values ($1,$2,$3) Returning *",[waktu, score, id_user]);
        res.status(200).json({
            status: 'Sukses Menambahkan Data',
            data: NewHistory.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.updateHistory = async (req, res) => {
    try{
        const {waktu, score, id_user} = req.body;
        const UpdateHistory = await pool.query("Update user_game_history Set waktu = ($1), score = ($2), id_user =($3) where id = ($4) Returning *",[waktu, score, id_user,req.params.id]);
        res.status(200).json({
            status: 'Sukses Update Data',
            data: UpdateHistory.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.deleteHistory = async (req,res) => {
    try {
        const deleteHistory = await pool.query("Delete from user_game_history where id = ($1)",[req.params.id]);
        res.status(200).json({
            status: 'Sucess Menghapus data'
        })
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message
        })
    }
}