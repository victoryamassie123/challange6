const pool = require('../database');

exports.getBiodata = async (req, res) => {

    try {
        const AllBiodata = await pool.query("Select * from user_game_biodata");
        console.log(AllBiodata.rows);
        res.status(200).json({
            status: 'Success',
            data: AllBiodata.rows
        });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        })
    }
}
exports.createBiodata = async (req, res) => {
    try{
        const {name, phone, address, id_user} = req.body;
        const NewBiodata = await pool.query("Insert into user_game_biodata (name, phone, address, id_user) Values ($1,$2,$3,$4) Returning *",[name, phone, address, id_user]);
        res.status(200).json({
            status: 'Sukses Menambahkan Data',
            data: NewBiodata.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.updateBiodata = async (req, res) => {
    try{
        const {name, phone, address, id_user} = req.body;
        const UpdateUser = await pool.query("Update user_game_biodata Set name = ($1), phone = ($2), address = ($3), id_user=($4) where id = ($5) Returning *",[name, phone, address, id_user,req.params.id]);
        res.status(200).json({
            status: 'Sukses Update Data',
            data: UpdateUser.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.deleteBiodata = async (req,res) => {
    try {
        const deleteUser = await pool.query("Delete from user_game_biodata where id = ($1)",[req.params.id]);
        res.status(200).json({
            status: 'Sucess Menghapus data'
        })
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message
        })
    }
}