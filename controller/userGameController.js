const pool = require('../database');

exports.getUser = async (req, res) => {
    // res.render('users', {
    //     layout: 'layouts/main-layout',
    //     nama: 'fadil',
    //     title: 'Daftar User'
    // });
    try {
        const AllUser = await pool.query("Select * from user_game");
        console.log(AllUser.rows);
        res.status(200).json({
            status: 'Success',
            data: AllUser.rows
        });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        })
    }
}
exports.createUser = async (req, res) => {
    try{
        const {username, password} = req.body;
        const NewUser = await pool.query("Insert into user_game (username, password) Values ($1,$2) Returning *",[username, password]);
        res.status(200).json({
            status: 'Sukses Menambahkan Data',
            data: NewUser.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.updateUser = async (req, res) => {
    try{
        const {username, password} = req.body;
        const UpdateUser = await pool.query("Update user_game Set username = ($1), password = ($2) where id = ($3) Returning *",[username, password,req.params.id]);
        res.status(200).json({
            status: 'Sukses Update Data',
            data: UpdateUser.rows
            });
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message 
        });
    }
}
exports.deleteUser = async (req,res) => {
    try {
        const deleteUser = await pool.query("Delete from user_game where id = ($1)",[req.params.id]);
        res.status(200).json({
            status: 'Sucess Menghapus data'
        })
    } catch (err) {
        res.status(500).json({
            status : 'Failed',
            message : err.message
        })
    }
}